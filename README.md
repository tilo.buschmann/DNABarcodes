# DNABarcodes

A copy of my DNABarcodes R project, mirrored from the official source (https://bioconductor.org/packages/release/bioc/html/DNABarcodes.html)

This code was developed as part of my PhD. Further information, additional concepts and methods may be found in my thesis: http://nbn-resolving.de/urn:nbn:de:bsz:15-qucosa-209812

A simple introduction to this package may be found here: https://academic.oup.com/bioinformatics/article/33/6/920/2804018
